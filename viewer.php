<?php session_start();?>

<html>
    <head>
        <title>UFX 4.0</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="js/viewer.js"></script>
        <script src="js/modal.js"></script>
        <link rel="stylesheet" href="css/modal.css">
        <link rel="stylesheet" href="css/layout.css">
        <link rel="stylesheet" href="css/table.css">
        <link rel="stylesheet" href="css/client_form.css">

        <style>
        .button {
            background-color: #ffffff;
            border: none;
            color: black;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        } 
        canvas.jSignature { height: 800px; }
        </style>
        
        <script src="js/libs/jsignature/jSignature.min.js"></script>
	<script>
	    $(document).ready(function() {
//	        $("#signature").jSignature();
                $('#signature').jSignature({ lineWidth: 2, width: 300, height: 200 });
	    })
	</script>
	       
    </head>
    <body>
        
        <div class="header">
            <h2>Viewers Home Page</h2>
        </div>
        <div class="content">
                <!-- notification message -->
                <?php if (isset($_SESSION['success'])) : ?>
              <div class="error success" >
                <h3>
                  <?php 
                        echo $_SESSION['success']; 
                        unset($_SESSION['success']);
                  ?>
                </h3>
              </div>
                <?php endif ?>

            <!-- logged in user information -->
            <?php  if (isset($_SESSION['username'])) : ?>
                <p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
                <p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
            <?php endif ?>
        </div>
        
        <!--<button class="button" value="unbooked">Leads</button>&nbsp;--><button class="button" value="booked">Bookings</button>&nbsp;<button class="button" value="no_sale">No Sale</button>&nbsp;<button class="button" value="all">All</button>
        
        <!-- The Modal -->
        <div id="myModal" class="modal">
          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">&times;</span>
              <h2>Booking for </h2>
            </div>
            <div class="modal-body">
              <p>Booking Form</p>
                <div class="container">
                    <form id="client_form">

                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" placeholder="Your name..">

                    <label for="date">Date</label>
                    <input type="text" id="date" name="date" placeholder="date">
                    <label for="date">Price</label>
                    <input type="text" id="date" name="date" placeholder="price">
                    
                    <input type="hidden" id="hiddenSigData" name="hiddenSigData" />
                    
                    <label for="yourSignature">Approver Signature</label>
                    <div id="signature"></div>
                      <button type="button" class="btn btn-default" onclick="$('#signature').jSignature('clear')">Clear</button>
                    <input type="submit" value="Submit">

                  </form>
                </div>
            </div>
            <div class="modal-footer">
              <h3>Modal Footer</h3>
            </div>
          </div>
        </div>
         
        <table>
	<thead>
	<tr>
            <th>Name</th>
            <th>Age</th>
            <th>Email</th>
            <th>Tel</th>
            <th>Address</th>
            <th></th>
	</tr>
	</thead>
        <tbody id="bookers_data_table">
	</tbody>
        </table>
    </body>
</html>
 