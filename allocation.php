<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "customers";
    
    if(isset($_GET['op'])){
        $op = $_GET['op'];
        $option = $_GET['leads'];    
        
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
    
        switch ($op) {
            case "r":
                $sql = '';
                switch ($option){
                    case 1:
                        $sql = "SELECT leads.id, CONCAT(f_name, ' ', l_name) as name, leads.date FROM leads LEFT JOIN bookings ON leads.id = bookings.customer_id LEFT JOIN tasks ON leads.id = task WHERE bookings.customer_id IS NULL AND task IS NULL";
                    break;
                    case 2:
                        $sql = "SELECT leads.id, CONCAT(f_name, ' ', l_name) as name, bookings.date, TIME_FORMAT(bookings.time, '%H:%i') AS time FROM leads JOIN bookings ON leads.id = bookings.customer_id JOIN tasks ON customer_id = task JOIN users ON tasks.user = users.id WHERE users.access = 1";
                    break;
                    case 3:
                        $sql = "SELECT leads.id, CONCAT(f_name, ' ', l_name) as name, leads.date AS cdate, bookings.date AS bdate, TIME_FORMAT(bookings.time, '%H:%i') AS time, users.username FROM leads LEFT JOIN bookings ON leads.id = bookings.customer_id JOIN tasks ON leads.id = task JOIN users ON tasks.user = users.id WHERE access = 1";
                    break;
                    case 4:
                        $sql = "SELECT leads.id, CONCAT(f_name, ' ', l_name) as name, bookings.date AS date, TIME_FORMAT(bookings.time, '%H:%i') AS time, users.username FROM leads JOIN bookings ON leads.id = bookings.customer_id JOIN tasks ON leads.id = task JOIN users ON tasks.user = users.id WHERE access = 2";
                    break;
                }

                $result = $conn->query($sql);                
                $data = [];

                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        if($option == 3 || $option == 4){$data[] = $row["username"];}
                        $data[] = $row["id"];
                        $data[] = $row["name"];
                        if($option == 3){$data[] = (isset($row["bdate"])) ? $row["bdate"] : $row["cdate"];}
                        else { $data[] = $row["date"];} 
                        if($option == 2 ||$option == 3 || $option == 4){$data[] = $row["time"];}                        
                    }
                } else {
                    echo "0 results";
                }
                header('Content-Type: application/json');
                echo json_encode($data);
            break;
            //auto allocate
            case "w":
                if(isset($_GET['leads']) && !isset($_GET['lead_id']) && !isset($_GET['user_id'])){
                    //leads for bookers and viewers respectively 
                    $sql_1 = "SELECT leads.id, CONCAT(f_name, ' ', l_name) as name, age, leads.date FROM leads LEFT JOIN bookings ON leads.id = bookings.customer_id LEFT JOIN tasks ON leads.id = task WHERE bookings.customer_id IS NULL AND task IS NULL";
                    $sql_2 = "SELECT leads.id, CONCAT(f_name, ' ', l_name) as name, age, bookings.date, bookings.time FROM leads JOIN bookings ON leads.id = bookings.customer_id JOIN tasks ON leads.id = task JOIN users ON tasks.user = users.id WHERE users.access = 1";

                    $sql = ($option < 2) ? $sql_1 : $sql_2;
                    $access = ($option < 2) ? 1 : 2;

                    $result = $conn->query($sql);

                    $sql_3 = "SELECT id FROM users WHERE access = $access";
                    $result2 = $conn->query($sql_3);

                    $users = $result2->num_rows;
                    $leads = $result->num_rows;

                    $leadspuser = $leads/$users;

                    $data = [];
                    $leads_data = [];

                    //get users
                    if ($result2->num_rows > 0) {
                        while($row = $result2->fetch_assoc()) {
                            $data[] = $row["id"];
                        }
                    } else {
                        echo "0 results";
                    }

                    //get leads
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $leads_data[] = $row["id"];
                        }
                    } else {
                        echo "0 results";
                    }


                    $iterations = $leadspuser * count($data);

                    $remainder = count($leads_data)%count($data);
                    for($i=0; $i<count($data); $i++){
                        for($j=0; $j<$leadspuser; $j++){
                            $c = ($i)*$leadspuser+$j;

                            $sql_1 ="INSERT INTO tasks (user, task, date, time) VALUES ('$data[$i]', '$leads_data[$c]', CURDATE(), CURTIME())";
                            $sql_2 ="UPDATE tasks SET user = $data[$i], `date` = CURDATE(), `time` = CURTIME() WHERE task = $leads_data[$c]";

                            $sql = ($option == 1) ? $sql_1 : $sql_2;

                            if ($conn->query($sql) === TRUE) { echo "New record created successfully";}
                                else {echo "Error: " . $sql . "<br>" . $conn->error;}
                        }
                    }

                    if($remainder>0){

                        for($i = 0; $i<$remainder;$i++){
                            $index=$iterations+$i;

                            $sql_1 ="INSERT INTO tasks (user, task, date, time) VALUES ('$data[$i]', '$leads_data[$index]', CURDATE(), CURTIME())";
                            $sql_2 ="UPDATE tasks SET user = $data[$i], `date` = CURDATE(), `time` = CURTIME()";

                            $sql = ($option == 1) ? $sql_1 : $sql_2;

                            if ($conn->query($sql) === TRUE) { echo "New record created successfully";}
                                else {echo "Error: " . $sql . "<br>" . $conn->error;}
                        }
                    }
                //individual allocation
                }elseif (isset($_GET['leads']) && isset($_GET['lead_id']) && isset($_GET['user_id'])) {
                    $lead = $_GET['lead_id'];
                    $user = $_GET['user_id'];
                    
                    $sql_1 ="INSERT INTO tasks (user, task, date, time) VALUES ('$user', '$lead', CURDATE(), CURTIME())";
                    $sql_2 ="UPDATE tasks SET user = $user, `date` = CURDATE(), `time` = CURTIME() WHERE task = $lead";

                    $sql = ($option == 1) ? $sql_1 : $sql_2;

                    if ($conn->query($sql) === TRUE) { echo "New record created/updated successfully";}
                        else {echo "Error: " . $sql . "<br>" . $conn->error;}
                }
            break;
        }
        mysqli_free_result($result);
        $conn->close();    
    }    
    
    
    
    
    
   
