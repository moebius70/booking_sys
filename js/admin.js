// 1 = unbooked leads
// 2 = booked leads
// 3 = booker with leads
// 4 = viewers with leads

var data;

$(document).ready(function(){
    jQuery("time.timeago").timeago();
    page_load('1');
    $('button').eq(0).css('color', 'red');
    $('button').eq(0).attr('disabled',true);
    update();

    $(document).on("click", ".button", function(e) {
        if($(this).val()!== 'auto'){
            $('.button').css('color', 'black');
            $('.button').attr('disabled',false);
            $(this).css('color', 'red');
            $(this).attr('disabled',true);
            page_load($(this).val());
        }
        if($(this).val()=== 'auto'){
            $('button').eq(4).mousedown(function(){
              $(this).css({
              'color': 'red'     
              });               
            }).mouseup(function(){
              $(this).css({
              'color': 'black'     
              });               
            }).mouseout(function(){
              $(this).css({
              'color': 'black'     
              });               
            });                        

            var request;
            var state = ($('button').eq(0).is(":disabled")) ? 1 : 2;
            request = $.ajax({  
                type: "GET",
                url: "allocation.php", 
                data: {
                    op: 'w',
                    leads: state 
                 },
                success:  function(data){
                            console.log("data2: "+data);
                            $('#bookers_data_table').empty();
                        }
            }); 
        }
    });
});

function page_load(view){
    $('#bookers_data_table').empty();
    $.ajax({  
        type: "GET",
        dataType: "JSON",
        url: "allocation.php", 
        data: {
            op: 'r',
            leads: view 
         },
        success: function(data){
                    create_view(data, view);
                }
    }); 
}    


function create_view(data, view){
    $('#bookers_data_table').empty();
    var th;// =  (view == 3 || view == 4) ? 5 : 3;
    if(view == 3 || view == 4){th=5;}
    else if(view == 2){th=4;}
    else{th=3;}
//    console.log("th: "+th);
    for(i=0; i<data.length; i+=th){
        switch (view) {
            case '1':
                    $('th').eq(0).text("Customer Name");
                    $('th').eq(1).text("Lead Date");
                    $('th').eq(2).text("");
                    $('th').eq(3).text("");
                    $('th').eq(4).text("Booker Name");
                    $('#bookers_data_table').append('<tr data-id="'+data[i]+'"><td>'+data[i+1]+'</td><td>'+sql2js(data[i+2])+'</td><td></td><td></td><td><select><option value="" disabled selected>Select your option</option></select></td></tr>');
                break;
            case '2':
                    $('th').eq(0).text("Customer Name");
                    $('th').eq(1).text("Appointment Date");
                    $('th').eq(2).text("Time");
                    $('th').eq(3).text("");
                    $('th').eq(4).text("Viewer Name");
                    $('#bookers_data_table').append('<tr data-id="'+data[i]+'"><td>'+data[i+1]+'</td><td>'+data[i+2]+'</td><td>'+data[i+3]+'</td><td></td><td><select><option value="" disabled selected>Select your option</option></select></td></tr>');
                break;
            case '3':
                    $('button').eq(4).attr('disabled',true);
                    $('button').eq(4).css('color', 'grey');
                    $('th').eq(0).text("Allocated User");
                    $('th').eq(1).text("Lead / Appointment Date");
                    $('th').eq(2).text("Time");
                    $('th').eq(3).text("Customer Name");
                    $('th').eq(4).text("Booker Name");
                    if(data[i+4]!== null){$('#bookers_data_table').append('<tr style="color: #2ECC71" data-id="'+data[i+1]+'"><td>'+data[i]+'</td><td>'+data[i+3]+'</td><td>'+data[i+4]+'</td><td>'+data[i+2]+'</td><td><select><option value="" disabled selected>Select your option</option></select></td></tr>');}
                    else{$('#bookers_data_table').append('<tr data-id="'+data[i+1]+'"><td>'+data[i]+'</td><td>'+sql2js(data[i+3])+'</td><td>--</td><td>'+data[i+2]+'</td><td><select><option value="" disabled selected>Select your option</option></select></td></tr>');}
                break;
            case '4':
                    $('button').eq(4).attr('disabled',true);
                    $('button').eq(4).css('color', 'grey');
                    $('th').eq(0).text("Allocated User");
                    $('th').eq(1).text("Lead Date");
                    $('th').eq(2).text("Time");
                    $('th').eq(3).text("Customer Name");
                    $('th').eq(4).text("Viewer Name");
                    
                    $('#bookers_data_table').append('<tr data-id="'+data[i+1]+'"><td>'+data[i]+'</td><td>'+data[i+3]+'</td><td>'+data[i+4]+'</td><td>'+data[i+2]+'</td><td><select><option value="" disabled selected>Select your option</option></select></td></tr>');
                break;
        }
    }
    get_users(view);
    allocate_task();
    $('#lead_count').text("Leads: "+count_leads());
}




function get_users(leads){
    var access = (leads == 1 || leads == 3) ? 1 : 2;
    $.ajax({  
        type: "GET",
        dataType: "JSON",
        url: "users.php", 
        data: { 
            users: access 
         },
        success:  function(data){
                    var str = '';
                    str = '<option value="" disabled selected>Select your option</option>';
                    for(i=0; i<data.length; i+=3){
                        str = str + '<option value="'+data[i]+'">'+data[i+1]+'</option>';
                    }
                    for(i=0; i<$('#bookers_data_table tr').length; i++){
//                      $('select').eq(i).append(str);
                      $('select').eq(i)
                      .find('option')
                      .remove()
                      .end()
                      .append(str);
                    }
                }
    }); 


}
//individual tasks
// 1 - insert
// 2 - update
// 3 - update
// 4 - update


function allocate_task(){
    $('select').on('change', function() {
        var user_id = $(this).val();
        var lead_id = $(this).closest('tr').data('id');
        var tr = $(this).closest('tr');
//                    var state = ($('button').eq(0).is(":disabled")) ? 'unbooked' : 'booked';
        var state = ($('button').eq(0).is(":disabled")) ? 1 : 2;
        $.ajax({  
            type: "GET",
            url: "allocation.php", 
            data: {
                op: 'w',
                leads: state,
                user_id: user_id,
                lead_id: lead_id
             },
            success:  function(data){
                        console.log("done");
                        tr.remove();
                      }
        }); 
    });
}


function count_leads(){
    var row_count = $('#bookers_data_table tr').length;
    return row_count;
}

//converts sql timestamp to js timestamp
function sql2js(te){
    // Split timestamp into [ Y, M, D, h, m, s ]
    //var t = "2010-06-09 13:12:01".split(/[- :]/);
    var t = te.split(/[- :]/);
    // Apply each element to the Date function
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]).toISOString();
    var a = jQuery.timeago(d);
    console.log(a);
    // -> Wed Jun 09 2010 14:12:01 GMT+0100 (BST)
    return a;
}

//uses sse events from database to update the page
function update(){
    var source = new EventSource("update.php");
    source.addEventListener("newlead", function(e) {            
        console.log(e.data);
         var obj = JSON.parse(e.data);
        $('#bookers_data_table').append('<tr data-id="'+obj.id+'"><td>'+obj.name+'</td><td>'+sql2js(obj.date)+'</td><td></td><td></td><td><select><option value="" disabled selected>Select your option</option></select></td></tr>');
        get_users(1);
        str = "Leads: ";// + obj.rows;
        str += ( count_leads() < obj.rows ) ? count_leads() : obj.rows;
        $('#lead_count').text(str);
    }, false);    
}

