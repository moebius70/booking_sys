$(document).ready(function(){
    page_load("1");
    $('button').eq(0).css('color', 'red');
    $('button').eq(0).attr('disabled',true);


    $('.button-container').children('button').attr('disabled',true);

//                var date_input = $('input[type="date"]');
//                date_input.valueAsDate = new Date();
    get_date();

    $('input[type="date"]').change(function(){
        var date = this.value;
        console.log(date);
        $('.button-container').children('button').attr('disabled',false);
        $.ajax({  
            type: "GET",
            dataType: "JSON",
            url: "booking.php",
            data: { 
                    op: 'r',
                    date: date,
                    query: 5 
             },
            success:  function(data){
                        console.log(data);
                        var concurrent_time_slots = 1;
                        for(i=0; i<data.length; i+=2){
                            if(data[i+1] == concurrent_time_slots){
                                $('.button-container').children('button').eq(ts2bi(data[i])).attr('disabled',true);
                            }
                        }   
                    }
        }); 
   });

    $(document).on("click", ".button", function(e) {
        console.log("pressed");
        $('.button').css('color', 'black');
        $('.button').attr('disabled',false);
        $(this).css('color', 'red');
        $(this).attr('disabled',true);
        page_load($(this).val());

    });

   $(document).on("click", "button", function(e) {
       if($(this).parent()[0].className === 'button-container' && $('input[type=date]').val() !== ''){
           var datetime = $('input[type="date"]').val()+ "T" + $(this).val();
           $('input[name="daytime"]').val(datetime);
           console.log($('input[name=daytime]').val()+"\n");
       }
   });


});


function page_load(view){
    $('#bookers_data_table').empty();
//                console.log(view);
     // Variable to hold request
    var request;

    $.ajax({  
        type: "GET",
        dataType: "JSON",
        url: "booking.php",
        data: { 
                op: 'r',
                query: view 
         },
        success:  function(data){
//                                console.log(data);
                    create_view(data, view);
                    modal_data();
                    no_sale();
                    booking_form(request);
                }
    }); 
}    


function create_view(data, view){            
    for(i=0; i<data.length; i+=12){
        switch (view) {
            case "1":
                    $('th').eq(0).text("Name");
                    $('th').eq(1).text("Lead Date");
                    $('th').eq(2).text("Time");
                    $('th').eq(3).text("Tel");
                    $('th').eq(4).text("Viewer Name");                            
                    $('#bookers_data_table').append('<tr><td data-id="'+data[i]+'">'+data[i+2]+'</td><td><span>'+sql2js(data[i+1])+'</span></td><td><span>--</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn">Make a booking</button>&nbsp;<button id="myBtn2">No Sale</button></td></tr>');
                break;
            case "2":
//                            if(data[i+11]!= null && data[i+13]!== null){
                    $('th').eq(1).text("Date");
                    $('th').eq(2).text("Time");
                    $('#bookers_data_table').append('<tr><td data-id="'+data[i]+'">'+data[i+2]+'</td><td><span>'+data[i+10]+'</span></td><td><span>'+data[i+11]+'</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn">Edit booking</button>&nbsp;<button id="myBtn2">Cancel</button></td></tr>');
//                            }
                break;
            case "no_sale":
                if(data[i+10]!= null && data[i+12]=== null){
                    $('th').eq(1).text("Age");
                    $('th').eq(2).text("Email");                                
                    $('#bookers_data_table').append('<tr><td><span data-id="'+data[i]+'">'+data[i+2]+'</span></td><td><span>'+data[i+3]+'</span></td><td><span>'+data[i+4]+'</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn">Make a booking</button>&nbsp;<button onclick="restore('+this+')">Restore</button></td></tr>');
                }
                break;
            case "4":
                $('th').eq(0).text("Name");
                $('th').eq(1).text("Lead/Appointment Date");
                $('th').eq(2).text("Time");
                $('th').eq(3).text("Tel");
                $('th').eq(4).text("Viewer Name");                            
                if(data[i+11]!== null){$('#bookers_data_table').append('<tr style="color: #2ECC71"><td><span data-id="'+data[i]+'">'+data[i+2]+'</span></td><td><span>'+data[i+10]+'</span></td><td><span>'+data[i+11]+'</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn">Edit booking</button>&nbsp;<button id="myBtn2">No Sale</button></td></tr>');}
                else{$('#bookers_data_table').append('<tr><td><span data-id="'+data[i]+'">'+data[i+2]+'</span></td><td><span>'+sql2js(data[i+1])+'</span></td><td><span>--</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn">Make a booking</button>&nbsp;<button id="myBtn2">No Sale</button></td></tr>');}
        }
    }
}


function modal_data(){
    $(document).on("click", "button", function(e) {
        if($(this).closest('tbody').attr('id')==="bookers_data_table"){
            var name = $(this).closest('tr').children('td').eq(0).text();
            var id = $(this).closest('tr').children('td').eq(0).data('id');
            console.log("id: "+id+" name: "+name);
            $("h2").eq(1).text("Booking for "+ name);
            $('input[name="id"]').val(id);
        }
    }
);}

function no_sale(){

    $(document).on("click", "#myBtn2", function(e) {

    var n = ($("button").index(this)-3)/2; //n is the nth term in arithmentic sequence
    var span_index = 1+6*(n - 1);
    var id =  $("span").eq(span_index).data('id');
    //$(this).closest("tr").remove();

        request = $.ajax({
            url: "booking.php",
            type: "POST",
            data: {id: id,
                   op: 'w'
            }
        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
            // Log a message to the console
//                        console.log("Hooray, it worked!");
            $('span[data-id="'+ id +'"]').closest("tr").remove();

        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // Log the error to the console
            console.error(
                "The following error occurred: "+
                textStatus, errorThrown
            );
        });
    }
);}


// Callback handler that will be called regardless
// if the request failed or succeeded
function booking_form(request){
// Bind to the submit event of our form
$("#booking_form").submit(function(event){

    event.preventDefault();
    // Abort any pending request
    if (request) {
        request.abort();
    }
    // setup some local variables
    var $form = $(this);

    // Let's select and cache all the fields
    var $inputs = $form.find("input, select, button, textarea");
    //var $inputs = $form.find("input, button");

    // Serialize the data in the form
    var serializedData = "op=w&" + $form.serialize();
    // Let's disable the inputs for the duration of the Ajax request.
    // Note: we disable elements AFTER the form data has been serialized.
    // Disabled form elements will not be serialized.
    $inputs.prop("disabled", true);

    // Fire off the request to /form.php
    var id = $('#myModal').find("[name=id]").val();                
//                console.log("serializedData: "+serializedData);
    request = $.ajax({
        url: "booking.php",
        type: "post",
        data: serializedData
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        //console.log("Hooray, it worked!");
         $('#myModal').removeAttr("style").hide();
         //checks if it's an update or a new booking
         if($('th').eq(1).text()!="Date"){$('td[data-id="'+ id +'"]').closest("tr").remove();}
         else{
                s = $('input').eq(2).val().split('T');
//                            var ind = $('span[data-id="'+ id +'"]').closest("td").index();
                var ind = $('td[data-id="'+ id +'"]').closest("td").index();
                console.log("the index is: "+ind);
                $('td').eq(ind+1).text(s[0]);
                $('td').eq(ind+2).text(s[1]);
         }
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

    request.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
    });

    });                                   
}
        
function get_date(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd;
    } 

    if(mm<10) {
        mm = '0'+mm;
    } 

    //today = mm + '/' + dd + '/' + yyyy;
//            today = yyyy + '-' + mm + '-' + dd+"T00:00";
    today = yyyy + '-' + mm + '-' + dd;
//            $("#daytime").attr({
    $("input[type='date']").attr({    
        'min': today
   });
}
        
//converts time to button index number
function ts2bi(hms){
    var start_time = 600; //600 minutes = 10am
    var time_slot_size = 30; //size of time slot in minutes
    var a = hms.split(':'); // split time at the colons
    var minutes = (+a[0]) * 60 + (+a[1]);// Hours are worth 60 minutes.
    var time_slots = (minutes - start_time) / time_slot_size; // number of time slots
    console.log("time slot: "+time_slots+"\n");
    return time_slots;
}

//converts sql timestamp to js timestamp
function sql2js(te){
    // Split timestamp into [ Y, M, D, h, m, s ]
    //var t = "2010-06-09 13:12:01".split(/[- :]/);
    var t = te.split(/[- :]/);
    // Apply each element to the Date function
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]).toISOString();
    var a = jQuery.timeago(d);
    console.log(a);
    // -> Wed Jun 09 2010 14:12:01 GMT+0100 (BST)
    return a;
}
