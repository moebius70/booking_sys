            $(document).ready(function(){
                page_load("booked");
                $('button').eq(0).css('color', 'red');
                $('button').eq(0).attr('disabled',true);
                
                $(document).on("click", ".button", function(e) {
                    console.log("pressed");
                    $('.button').css('color', 'black');
                    $('.button').attr('disabled',false);
                    $(this).css('color', 'red');
                    $(this).attr('disabled',true);
                    page_load($(this).val());
                });

                $("#client_form").submit(function(event){
                    var sigData = $('#signature').jSignature('getData', 'base30');
                    $('#hiddenSigData').val(sigData);
                    console.log('jSignature saving signature: '+ sigData);
                    
                    event.preventDefault();
                    // Abort any pending request
//                    if (request) {
//                        request.abort();
//                    }
                    // setup some local variables
                    var $form = $(this);

                    var $inputs = $form.find("input, select, button, textarea");
                    //var $inputs = $form.find("input, button");

                    var serializedData = "op=w&" + $form.serialize();
                    $inputs.prop("disabled", true);

                    var id = $('#myModal').find("[name=id]").val();                
    //                console.log("serializedData: "+serializedData);
                    request = $.ajax({
                        url: "viewerbe.php",
                        type: "post",
                        data: serializedData
                    });
                    
                    
                });                
            });
            
            
            function page_load(view){
                $('#bookers_data_table').empty();

                 // Variable to hold request
                var request;

                $.ajax({  
                    type: "GET",
                    dataType: "JSON",
                    url: "viewerbe.php",
                    data: { 
                            op: 'r',
                            query: 2 
                     },
                    success:  function(data){
//                                console.log(data);
                                create_view(data, view);
//                                modal_data();
//                                no_sale();
//                                booking_form(request);
                            }
                }); 
            }    
            
            
            function create_view(data, view){            
                for(i=0; i<data.length-11; i+=13){
                    switch (view) {
//                        case "unbooked":
//                            if(data[10+13*(i/13)]=== null){
//                                $('th').eq(1).text("Age");
//                                $('th').eq(2).text("Email");
//                                $('#bookers_data_table').append('<tr><td><span data-id="'+data[i]+'">'+data[i+2]+'</span></td><td><span>'+data[i+3]+'</span></td><td><span>'+data[i+4]+'</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn">Make a booking</button>&nbsp;<button id="myBtn2">No Sale</button></td></tr>');
//                            }
//                            break;
                        case "booked":
                            if(data[i+11]!= null && data[i+13]!== null){
                                $('th').eq(1).text("Date");
                                $('th').eq(2).text("Time");
                                $('#bookers_data_table').append('<tr><td><span data-id="'+data[i]+'">'+data[i+2]+'</span></td><td><span>'+data[i+11]+'</span></td><td><span>'+data[i+12]+'</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn" style="margin-right:5px">Open Form</button><button id="myBtn2">Cancel</button></span></td></tr>');
                            }
                            break;
                        case "no_sale":
                            if(data[i+10]!= null && data[i+12]=== null){
                                $('th').eq(1).text("Date");
                                $('th').eq(2).text("Time");
                                $('#bookers_data_table').append('<tr><td><span data-id="'+data[i]+'">'+data[i+2]+'</span></td><td><span>'+data[i+3]+'</span></td><td><span>'+data[i+4]+'</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn">Make a booking</button>&nbsp;<button onclick="restore('+this+')">Restore</button></td></tr>');
                            }
                            break;
                        case "all":
                            $('#bookers_data_table').append('<tr><td><span data-id="'+data[i]+'">'+data[i+2]+'</span></td><td><span>'+data[i+11]+'</span></td><td><span>'+data[i+12]+'</span></td><td><span>'+data[i+5]+'</span></td><td><span>'+data[i+8]+'</span></td><td><span><button id="myBtn2">No Sale</button></span></td></tr>');
                    }
                }
            }
            
            
//            function modal_data(){
//                $(document).on("click", "button", function(e) {
//                var n = ($("button").index(this)-2)/2; //n is the nth term in arithmentic sequence
//                var span_index = 1+6*(n - 1);
//                var name =  $("span").eq(span_index).text();
//                var id =  $("span").eq(span_index).data('id');
//
//                $("h2").eq(0).text("Booking for "+ name);
//                $('input[name="id"]').val(id);
//                }
//            );}

            function no_sale(){

                $(document).on("click", "#myBtn2", function(e) {

                var n = ($("button").index(this)-3)/2; //n is the nth term in arithmentic sequence
                var span_index = 1+6*(n - 1);
                var id =  $("span").eq(span_index).data('id');
                //$(this).closest("tr").remove();

                    request = $.ajax({
                        url: "viewer.be",
                        type: "POST",
                        data: {id: id,
                               op: 'w'
                        }
                    });

                    // Callback handler that will be called on success
                    request.done(function (response, textStatus, jqXHR){
                        // Log a message to the console
//                        console.log("Hooray, it worked!");
                        $('span[data-id="'+ id +'"]').closest("tr").remove();
                        
                    });

                    // Callback handler that will be called on failure
                    request.fail(function (jqXHR, textStatus, errorThrown){
                        // Log the error to the console
                        console.error(
                            "The following error occurred: "+
                            textStatus, errorThrown
                        );
                    });
                }
            );}
