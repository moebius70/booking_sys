<?php 
  session_start(); 

  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
    <title>UFX 3.0</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/ufx_3.js"></script>
    <script src="js/modal.js"></script>
    <link rel="stylesheet" href="css/modal.css">
    <link rel="stylesheet" href="css/layout.css">
    <link rel="stylesheet" href="css/table.css">

    <style>
    .button {
        background-color: #ffffff;
        border: none;
        color: black;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }

    </style>
        
        
</head>
<body>

<div class="header">
	<h2>Home Page</h2>
</div>
<div class="content">
  	<!-- notification message -->
  	<?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h3>
          <?php 
          	echo $_SESSION['success']; 
          	unset($_SESSION['success']);
          ?>
      	</h3>
      </div>
  	<?php endif ?>

    <!-- logged in user information -->
    <?php  if (isset($_SESSION['username'])) : ?>
    	<p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
    	<p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
    <?php endif ?>
</div>
    
        <button class="button" value="unbooked">Leads</button>&nbsp;<button class="button" value="booked">Bookings</button>&nbsp;<button class="button" value="no_sale">No Sale</button>&nbsp;<button class="button" value="all">All</button>
        
        
        <!-- The Modal -->
        <div id="myModal" class="modal">
          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">&times;</span>
              <h2>Booking for </h2>
            </div>
            <div class="modal-body">
              <p>Booking Form</p>
              <form id="booking_form">
                  <p>Enter date and time &nbsp;<input id="daytime" type="datetime-local" name="daytime" onclick="get_date()">&nbsp;&nbsp;
                  <input type="hidden" name="id" value=""  />
                  <input type="submit" value="Send" /></p>
              </form>
            </div>
            <div class="modal-footer">
              <h3>Modal Footer</h3>
            </div>
          </div>
        </div>
         
        <table>
	<thead>
	<tr>
            <th>Name</th>
            <th>Age</th>
            <th>Email</th>
            <th>Tel</th>
            <th>Address</th>
            <th></th>
	</tr>
	</thead>
        <tbody id="data_table">
	</tbody>
        </table>
    

</body>
</html>

