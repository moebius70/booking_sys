<?php 
  session_start(); 

//  if (!isset($_SESSION['username'])) {
//  	$_SESSION['msg'] = "You must log in first";
//  	header('location: login.php');
//  }
//  if (isset($_GET['logout'])) {
//  	session_destroy();
//  	unset($_SESSION['username']);
//  	header("location: login.php");
//  }
?>


<html>
    <head>
        <title>UFX 4.0</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="js/admin.js"></script>
        <script src="js/jquery.timeago.js" type="text/javascript"></script>        
        <link rel="stylesheet" type="text/css" href="css/style.css">
        
        <link rel="stylesheet" href="css/layout.css">
        <link rel="stylesheet" href="css/table.css">

        <style>
        .button {
            background-color: #ffffff;
            border: none;
            color: black;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
        
        .btn:active { 
            color: red;        
        }
        
        .rightside {
            float: right;
            position: absolute;
            top: 50px;
            right: 450px;
            font-size: 22px;
            font-family: "Arial", "Helvetica"; 
        }

        </style>

    </head>
        

    <body>
        <div class="header">
            <h2>Admin Home Page</h2>
        </div>
        <div class="content">
                 notification message 
                <?php //  if (isset($_SESSION['success'])) : ?>
              <div class="error success" >
                <h3>
                  <?php 
//                        echo $_SESSION['success']; 
//                        unset($_SESSION['success']);
                  ?>
                </h3>
              </div>
                <?php // endif ?>

             logged in user information 
            <?php //  if (isset($_SESSION['username'])) : ?>
                <p>Welcome <strong><?php // echo $_SESSION['username']; ?></strong></p>
                <p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
            <?php // endif ?>
        </div>
        <div class="rightside" id="lead_count"></div>
        
        
        
        <button class="button" value="1">Leads</button>&nbsp;<button class="button" value="2">Bookings</button>
        <button class="button" value="3">Bookers</button>&nbsp;<button class="button" value="4">Viewers</button>&nbsp;
        <button class="button btn" style="float: right" value="auto">Auto Allocate</button>
                 
        <table>
	<thead>
	<tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
	</tr>
	</thead>
        <tbody id="bookers_data_table">
	</tbody>
        </table>

        
    </body>
</html>
