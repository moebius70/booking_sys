<?php

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "customers";

    if(isset($_REQUEST['option'])){

        $option = $_REQUEST['option'];
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 


        $sql_1 = "INSERT INTO leads (date, f_name, l_name, age, email, mobile, home, p_number, address, postcode, photo)
        VALUES (NOW(), 'Ernst', 'Blowfeld', '49', 'jbond92@gmail.com', '07968779012', '07968779012', '07968779012', '58 grosvenor rd, London', 'N3 1EX', 'uploads/ernst')";

        $sql_2 = "INSERT INTO leads (date, f_name, l_name, age, email, mobile, home, p_number, address, postcode, photo)
        VALUES (NOW(), 'Ernst', 'Blowfeld', '49', 'jbond92@gmail.com', '07968779012', '07968779012', '07968779012', '58 grosvenor rd, London', 'N3 1EX', 'uploads/ernst');";
        $sql .= "INSERT INTO leads (date, f_name, l_name, age, email, mobile, home, p_number, address, postcode, photo)
        VALUES (NOW(), 'Isaac', 'Asimov', '52', 'jbond92@gmail.com', '07968779012', '07968779012', '07968779012', '58 grosvenor rd, London', 'N3 1EX', 'uploads/albert');";
        $sql .= "INSERT INTO leads (date, f_name, l_name, age, email, mobile, home, p_number, address, postcode, photo)
        VALUES (NOW(), 'Ray', 'Bradbury', '91', 'jbond92@gmail.com', '07968779012', '07968779012', '07968779012', '58 grosvenor rd, London', 'N3 1EX', 'uploads/akira')";
        
        
        $sql_3 = "DELETE FROM leads ORDER BY id DESC limit 1";
        
        $sql = ($option === 'insert') ? $sql_1 : $sql_3;
        
        
        if ($conn->multi_query($sql) === TRUE) {
            $msg_1 =  "New record created successfully";
            $msg_2 =  "Record deleted successfully";
            $msg = ($option === 'insert') ? $msg_1 : $msg_2;
            echo $msg;
//            header('Content-Type: application/json');
//            echo json_encode($msg);            
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();
    }
        
?>


<html>
    <head>
        <title>SSE Tester UFX 5.0</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript">
            function del(){
                $('input[name="option"]').val('delete');
                $('form').submit();
            }
            
            function ins(){
                $('input[name="option"]').val('insert');
                $('form').submit();
            }
            
        </script>
    </head>
    <body>
        <div>
            <h2>Test SSE</h2>
            <form method="GET" action="ssetester.php">
                <input type="hidden" name="option"/>
                <input type="submit" value="Insert" onclick="ins()"/>
                <input type="submit" value="Delete" onclick="del()"/>                
            </form>
        </div>
    </body>
</html>