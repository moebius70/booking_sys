<?php
header("Content-Type: text/event-stream\n\n");
header("Cache-Control: no-cache");

function connect() {
    // set connection
    // use localhost as for testing only
    $con = mysqli_connect( 'localhost','root','' );
    mysqli_select_db( $con,'customers' );

    return $con;
}

$conn = connect();


//$id_sql = "SELECT MAX(leads.id) AS id FROM customer";
$data = [];
$id_sql = "SELECT MAX(leads.id) AS id FROM leads UNION SELECT COUNT(*) FROM leads LEFT JOIN bookings ON leads.id = bookings.customer_id LEFT JOIN tasks ON leads.id = task WHERE bookings.customer_id IS NULL AND task IS NULL";
$result = $conn->query($id_sql);
while($row = mysqli_fetch_assoc($result)){
   $data[] = $row['id']; 
}
$result->free();

$id = $data[0];
$cnt = $data[1];

unset($data);

while (TRUE){
    $sql = "SELECT leads.id AS id, CONCAT(f_name, ' ', l_name) AS name, leads.date AS date FROM leads LEFT JOIN bookings ON leads.id = bookings.customer_id LEFT JOIN tasks ON leads.id = task WHERE bookings.customer_id IS NULL AND task IS NULL AND leads.id > $id";
    $result = $conn->query($sql); 

    $cnt += $result->num_rows;
    
    while($data = mysqli_fetch_assoc($result)){
        $id = $data['id'];
        $data['rows'] = $cnt;
        $str = json_encode($data);
        echo "event: newlead\n";
        echo "id: ".$id."\n";
        echo "data: {$str}\n\n";
        ob_end_flush();
//	ob_flush();
        flush();		
    }
    $result->free();
    sleep(2);  
}

?>