<?php
session_start();
    $user_id = $_SESSION['user_id'];

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "customers";

    if(isset($_REQUEST['op'])){
        
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        
        $op = $_REQUEST['op'];
        !isset($_POST['id']) ?: $id = $_POST['id'];
        
        switch ($op) {
            case "r":

            $query = $_REQUEST['query'];

            $sql_1 = "SELECT id, leads.date, CONCAT(f_name, ' ', l_name) as name, age, email, mobile, home, p_number, CONCAT(address, ' ', postcode) as address, photo, booking_id, bookings.date AS booked_date, TIME_FORMAT(bookings.time, '%H:%i') AS booked_time FROM leads LEFT JOIN bookings ON leads.id = bookings.customer_id";
            $sql_2 = "SELECT leads.id, leads.date, CONCAT(f_name, ' ', l_name) as name, age, leads.email, mobile, home, p_number, CONCAT(address, ' ', postcode) as address,"
                . " photo, booking_id, bookings.date AS booked_date, TIME_FORMAT(bookings.time, '%H:%i') AS booked_time "
                . "FROM leads JOIN bookings ON leads.id = bookings.customer_id JOIN tasks ON task = leads.id JOIN users ON tasks.user = users.id WHERE task IN (SELECT task FROM tasks WHERE user = $user_id) AND users.access = 2";

            $sql = ($query == 1) ? $sql_1 : $sql_2; 

                $result = $conn->query($sql);

                $data = [];

                if ($result->num_rows > 0) {
                // output data of each row
                    while($row = $result->fetch_assoc()) {
                        $data[] = $row["id"];
                        $data[] = $row["date"];
                        $data[] = $row["name"];
                        $data[] = $row["age"];
                        $data[] = $row["email"];
                        $data[] = $row["mobile"];
                        $data[] = $row["home"];
                        $data[] = $row["p_number"];
                        $data[] = $row["address"];
                        $data[] = $row["photo"];
                        $data[] = $row["booking_id"];
                        $data[] = $row["booked_date"];
                        $data[] = $row["booked_time"];
                    }
                } else {
                    echo "0 results";
                }
            header('Content-Type: application/json');
            echo json_encode($data);
        break;
        
        case "w":
            
            !isset($_POST['name']) ?: $name = $_POST['name'];
            !isset($_POST['l_name']) ?: $l_name = $_POST['l_name'];
            !isset($_POST['date']) ?: $date = $_POST['date'];
            !isset($_POST['time']) ?: $time = $_POST['time'];
            !isset($_POST['payment']) ?: $payment = $_POST['payment'];
            !isset($_POST['hiddenSigData']) ?: $signature = $_POST['hiddenSigData'];
            
            $sql = "INSERT INTO customer (f_name, l_name, date, time, payment, signature) VALUES('name', 'name', CURDATE(), CURTIME(), '200', '$signature')";
            if (!mysqli_query($conn, $sql)) { echo "Multi query failed: (" . $mysqli->errno . ") " . $mysqli->error;}
        }
    mysqli_free_result($result); 
    $conn->close();        
    }

?>
