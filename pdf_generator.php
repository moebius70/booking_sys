<?php

require('lib/fpdf/fpdf.php'); 
include 'lib/base30toPNG.php';

//$imgStr='image/jsignature;base30,7U010100010000000001_1G88b8ace99aab756856_bE2000000010000000101_1D6689cbaa9b956558564_8w757698987766566556545_3PZ2110101010100000000Y10_fF0000Z2100001000Y110_1V9789cb86966655475_fK_1x';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "customers";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT CONCAT(f_name, ' ', l_name) AS name, payment, date, time, signature FROM customer";
$result = $conn->query($sql);
$data = "";

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $imgStr = $row["signature"];
        $name = $row["name"];
        $payment = $row["payment"];
        $date = $row["date"];
        $time = $row["time"];
    }
} else {
         echo "0 results";
        }

//if(isset($_REQUEST['name'])){
//
//    $name = $_REQUEST['name'];

    $pdf = new FPDF('P', 'pt', 'A4');

    $pdf->AddPage(); 
    $pdf->SetFont('Arial', 'B', 22);
    $pdf->Cell(100, 16, "PHOTOGRAPHIC IMAGES");
    $pdf->Ln(100);
    $pdf->SetFont('Arial', '', 12); 
    $pdf->Cell(100, 13, date('F j, Y'), 0, 1);
    $pdf->Ln(100);
    $pdf->Cell(100, 13, "Name: ".$name);
    $pdf->Ln(15);
    $pdf->Cell(100, 13, "Payment: GBP".$payment);
    $pdf->Ln(200);
    
    $pdf->Cell(100, 0, "Signature");
    $pdf->Ln(100);
    
//    $pdf->Image('chart.png',10,200,-300);
//    $pdf->Image(base30_to_jpeg($imgStr, 'test.png'),10,200,100);
    $pdf->Image(base30_to_jpeg($imgStr, 'test.png'),10,400,100);
    $pdf->Output();
    
//}
    

