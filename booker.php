<?php session_start();?>

<html>
    <head>
        <title>UFX 4.0</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="js/jquery.timeago.js" type="text/javascript"></script>
        <script src="js/booker.js"></script>
        <script src="js/modal.js"></script>
        <link rel="stylesheet" href="css/modal.css">
        <link rel="stylesheet" href="css/layout.css">
        <link rel="stylesheet" href="css/table.css">

        <style>
        .button {
            background-color: #ffffff;
            border: none;
            color: black;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .button-container {
            width: 220px;
            background: white;
            overflow-y: auto;
            margin-top: 15px;            
        }

        .button-container > button {
            width: 80px;
            height: 20px;
            float: left;
            background: lightgray;   
            margin: 5px;
        }                   
        </style>
                    
    </head>

    <body>
        
        <div class="header">
            <h2>Bookers Home Page</h2>
        </div>
        <div class="content">
                <!-- notification message -->
                <?php if (isset($_SESSION['success'])) : ?>
              <div class="error success" >
                <h3>
                  <?php 
                        echo $_SESSION['success']; 
                        unset($_SESSION['success']);
                  ?>
                </h3>
              </div>
                <?php endif ?>

            <!-- logged in user information -->
            <?php  if (isset($_SESSION['username'])) : ?>
                <p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
                <p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
            <?php endif ?>
        </div>
        
        
        
        <button class="button" value="1">Leads</button>&nbsp;<button class="button" value="2">Bookings</button>&nbsp;<button class="button" value="3">No Sale</button>&nbsp;<button class="button" value="4">All</button>
        
        <!-- The Modal -->
        <div id="myModal" class="modal">
          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">&times;</span>
              <h2>Booking for </h2>
            </div>
            <div class="modal-body">
              <p>Booking Form</p>
              <form id="booking_form">
                  <p>Enter date and time &nbsp;<input type="date" onclick="get_date()">&nbsp;&nbsp;
                  <input type="hidden" name="id" value=""  />
<!--                  <input type="submit" value="Send" /></p>-->
                  <div class="button-container">
                      <button type="button" value="10:00">1000</button>
                      <button type="button" value="10:30">1030</button>
                      <button type="button" value="11:00">1100</button>
                      <button type="button" value="11:30">1130</button>
                      <button type="button" value="12:00">1200</button>
                      <button type="button" value="12:30">1230</button>
                      <button type="button" value="13:00">1300</button>
                      <button type="button" value="13:30">1330</button>
                      <button type="button" value="14:00">1400</button>
                      <button type="button" value="14:30">1430</button>
                      <button type="button" value="15:00">1500</button>
                      <button type="button" value="15:30">1530</button>
                      <button type="button" value="16:00">1600</button>
                      <button type="button" value="16:30">1630</button>
                      <button type="button" value="17:00">1700</button>
                      <button type="button" value="17:30">1730</button>
                  </div>
                  <input type="hidden" name="daytime" value="" />
                  <input type="submit" value="Send" style="float: right;  border: solid 1px; width: 70px; height: 20px; position: absolute; top: 340px; left: 770px" />  
              </form>
            </div>
            <div class="modal-footer">
              <h3>Modal Footer</h3>
            </div>
          </div>
        </div>
        <!-- The Modal --> 
        
        <table>
	<thead>
	<tr>
            <th>Name</th>
            <th>Age</th>
            <th>Email</th>
            <th>Tel</th>
            <th>Address</th>
            <th></th>
	</tr>
	</thead>
        <tbody id="bookers_data_table">
	</tbody>
        </table>

        
        
    </body>
</html>
 